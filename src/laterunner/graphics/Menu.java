package laterunner.graphics;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;

import javax.swing.*;

import laterunner.core.GameEngine;
import laterunner.model.user.User;


/**
 * Menu is the class that displays the main menu and it's the first panel showed when the game starts.
 */
public class Menu extends PanelImpl {

    private static final long serialVersionUID = 1L;
    private static final int X = 72;
    private static final int Y = 268;
    private static final int INTER = 10;
    private static final int LEVELS = 11;
    private static final int SURVIVAL = 11;
    private static final int START_SCORE = 0;
    private ImageIcon playImage = super.getPics().getIcon(Icons.PLAY);
    private ImageIcon shopImage = super.getPics().getIcon(Icons.SHOP);
    private ImageIcon statsImage = super.getPics().getIcon(Icons.STATS);
    private ImageIcon quitImage = super.getPics().getIcon(Icons.QUIT);
    private GameEngine gameEngine;
    private Integer modality = 1;
    private static Integer levUnblocked;
    private static Font fontChalkDash;
    private static final float SIZE = 48;

    /**
     * Common Menu constructor. It has no layout and draws all the components relative to each other.
     * 
     * @param road
     *          an instance of Road class
     * @param gameEngine
     *          an instance of GameEngine class
     */
    public Menu(final Road road, final GameEngine gameEngine) {

        this.setLayout(null);
        fontChalkDash = super.createFont("Digital Dot Roadsign.otf", SIZE);
        this.gameEngine = gameEngine;
        updateLevel();

        //Combo box
        JComboBox<String> level = new JComboBox<>();
        level.setFont(fontChalkDash);

        DefaultListCellRenderer defaultListCellRenderer = new DefaultListCellRenderer();
        defaultListCellRenderer.setHorizontalAlignment(DefaultListCellRenderer.CENTER);
        level.setRenderer(defaultListCellRenderer);

            for (int n = 1; n <= LEVELS; n++) {
            if (n == SURVIVAL) {
                level.addItem("S");
            } else {
                level.addItem(Integer.toString(n));
            }
        }

        level.addItemListener(e -> {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                if (e.getItemSelectable().getSelectedObjects()[0].equals("S")) {
                    modality = SURVIVAL;
                } else {
                    modality = Integer.parseInt((String) e.getItemSelectable().getSelectedObjects()[0]);
                }
            }
        });

        level.setEditable(true);
        level.setOpaque(false);
        ((JTextField) level.getEditor().getEditorComponent()).setOpaque(false);
        level.setBounds(X + playImage.getIconWidth() + INTER, Y, playImage.getIconHeight(), playImage.getIconHeight());
        this.add(level);

        //Play button
        JButton play = super.createButton(playImage);
        play.setBounds(X, Y, playImage.getIconWidth(), playImage.getIconHeight());

        play.addActionListener(e -> {
            if (modality <= levUnblocked || modality == SURVIVAL) {
                this.gameEngine.setupLevel(modality, START_SCORE);
                SceneImpl.changePanel("road");
                new Thread(road).start();
            } else {
                JOptionPane.showMessageDialog(this, "You have to unblock it!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        //Shop button
       JButton shop = this.buttonFactory(shopImage,play.getBounds(),e -> SceneImpl.changePanel("shop"));

        //Stats button
        JButton stats=this.buttonFactory(statsImage,shop.getBounds(),e -> SceneImpl.changePanel("stats"));

        //Quit Button
        JButton quit = this.buttonFactory(quitImage,stats.getBounds(),e -> System.exit(0));
        this.add(play);
        this.add(shop);
        this.add(stats);
        this.add(quit);
    }

    private JButton buttonFactory(ImageIcon image, Rectangle position, ActionListener listener){
        JButton button =  super.createButton(image);
        button.setBounds(X, position.y + position.height + INTER,
                image.getIconWidth(), image.getIconHeight());
        button.addActionListener(listener);
        return button;
    }
    @Override
    protected void paintComponent(final Graphics g) {
        g.drawImage(super.getPics().getImage(Icons.MENU), 0, 0, null);
     }

    /**
     * Updates the number of the last level reached once the player has unlocked it.
     */
    public static void updateLevel() {
        levUnblocked = User.getUser().getLevelReached();
    }
}

