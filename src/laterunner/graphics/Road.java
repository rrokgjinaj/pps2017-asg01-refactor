package laterunner.graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.List;

import javax.swing.JLabel;

import laterunner.core.GameEngine;
import laterunner.model.user.User;
import laterunner.model.vehicle.Car;
import laterunner.model.vehicle.Obstacle;
import laterunner.model.vehicle.Vehicle;
import laterunner.model.world.GameState;
import laterunner.model.world.World;

/**
 * Road is the class that contains the real game. It displays all the vehicles and its elements are constantly repainted.
 *
 */
public class Road extends PanelImpl implements Runnable {

    private static final long serialVersionUID = 1L;
    private static final float SIZE = 60;
    private static final int STEP = -96;
    private static final int LCROSS = 372;
    private static final int RCROSS = 572;
    private static final int SPEEDFACTOR = 21;
    private static int SPEED = STEP;
    private Font fontChalkDash;
    private static JLabel label = new JLabel();
    private Audio audio = new Audio();

    private GameEngine gameEngine;
    private GameState gameState;

    /**
     * Common Road constructor: sets the GameEngine.
     * 
     * @param gmEngine
     *          an instance of GameEngine class
     */
    public Road(final GameEngine gmEngine) {
        this.gameEngine = gmEngine;
        fontChalkDash = super.createFont("Digital Dot Roadsign.otf", SIZE);
        label.setFont(fontChalkDash);
        label.setForeground(Color.BLACK);
        this.add(label, BorderLayout.NORTH);
    }

    /**
     * Paints Vehicles
     * @param graphics
     */
    @Override
    protected void paintComponent(final Graphics graphics) {
        super.paintComponent(graphics);

        World scene = gameState.getWorld();
        List<Vehicle> vehicles = scene.getSceneEntities();
        graphics.drawImage(super.getPics().getImage(Icons.ROAD), 0, 0, null);
        this.updateCross();
        graphics.drawImage(super.getPics().getImage(Icons.CROSS), LCROSS, SPEED, null);
        graphics.drawImage(super.getPics().getImage(Icons.CROSS), RCROSS, SPEED, null);

        vehicles.stream().forEach(vehicle -> {
            if (this.gameEngine.isSurvival()) {
                label.setText("<html><div style='text-align: center;'>" + "Score: " + gameState.getScore() + "</div></html>");
            } else {
            label.setText("<html><div style='text-align: center;'>" + "Lives: " + User.getUser().getUserLives()
                + "&#160; &#160; &#160; &#160; &#160;" + "Score: " + gameState.getScore() + "</div></html>");
            }
            if (vehicle instanceof Car) {
                Car car = (Car) vehicle;
                graphics.drawImage(super.getPics().getImage(Icons.CAR), (int) car.getCurrentPos().getX(),
                        (int) car.getCurrentPos().getY(), null);
            } else if (vehicle instanceof Obstacle) {
                Icons icon;
                switch (vehicle.getType()){
                    case MOTORBIKE:
                        icon=Icons.MOTORBIKE;
                    break;
                    case BUS:
                        icon=Icons.BUS;
                        break;
                    case OBSTACLE_CAR:
                        icon=Icons.JEEP;
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + ((Obstacle) vehicle).getType());
                }
                graphics.drawImage(super.getPics().getImage(icon), (int) vehicle.getCurrentPos().getX(),
                        (int) vehicle.getCurrentPos().getY(), null);
            }
        });
    }

    /**
     * Sets the GameState.
     * @param gmState
     *          an instance of GameState class
     */
    public void setGameState(final GameState gmState) {
        this.gameState = gmState;
    }

    @Override
    public void run() {
        gameEngine.mainLoop();
    }

    /**
     * updateCross
     */
    private void updateCross() {
        SPEED += SPEEDFACTOR;
        if (SPEED >= 0) {
                SPEED = STEP;
        }
    }

    /**
     * Returns an instance of Audio class.
     * @return
     *          an instance of Audio class
     */
    public Audio getAudio() {
        return this.audio;
    }

}