package laterunner.graphics;

import java.awt.Image;
import java.util.EnumMap;

import javax.swing.ImageIcon;

/**
 * Pictures Implementation.
 * 
 */
public class PicturesFunction implements Pictures {

    private EnumMap<Icons, ImageIcon> icons = new EnumMap<Icons, ImageIcon>(Icons.class);
    private EnumMap<Icons, Image> images = new EnumMap<Icons, Image>(Icons.class);

    /**
     * Common IconsFunction constructor. Initializes the maps containing all the images.
     */
    public PicturesFunction() {
        this.setMaps();
    }

    private static String getPath(final Icons icon) {
        String extension = ".png";
        if(icon== Icons.BACKGROUND) extension = ".jpg";
        return "/"+icon.toString().toLowerCase() + extension;

    }

    private static ImageIcon getEIcon(final Icons icon) {
        return new ImageIcon(Icons.class.getResource(getPath(icon)));
    }

    private static Image getEImage(final Icons icon) {
        return new ImageIcon(Icons.class.getResource(getPath(icon))).getImage();
    }

    private void setMaps() {
        for (Icons i:Icons.values()) {
            icons.put(i, getEIcon(i));
            images.put(i, getEImage(i));
        }
    }
    @Override
    public ImageIcon getIcon(final Icons icon) {
        return icons.get(icon);
    }

    @Override
    public Image getImage(final Icons icon) {
        return images.get(icon);
    }

}